def calculate_fuel_recursive(mass):
    total_fuel = 0
    for module_mass in mass:  # Iterates through each module in module list
        while module_mass > 0:  # Iterates through each module until a negative value is calculated
            module_mass = (int(int(module_mass) / 3) - 2)
            if module_mass > 0:  # If the value is positive add it to the total fuel
                total_fuel += module_mass
    return total_fuel


def calculate_fuel(mass):
    total_fuel = 0
    for module_mass in mass:
        total_fuel += (int(int(module_mass) / 3) - 2)
    return total_fuel


if __name__ == '__main__':
    raw_data = open("data.txt").readlines()  # Reads in .txt and turns it into a list
    mass_list = [int(i) for i in raw_data]  # Turns list of strings into a list of ints
    print(calculate_fuel(mass_list))
    print(calculate_fuel_recursive(mass_list))

    # Part One Solution 3152038
    # Part Two Solution 4725210
